class BiggerNumberOfFile

  def self.get_bigger_number(path)
    max_value = 0
    content = nil
    path.each_pair do |key, value|
      if value.count > max_value
        max_value = value.count
        content = value.first
      end
    end
    return "#{File.read(content)} #{max_value}"
  end

end
