class NumberOfFile
  require_relative 'bigger_number_of_files'

  def self.get_number_of_files
    folder_to_count = "/home/itmarketing/Downloads/DropsuiteTest/DropsuiteTest"
    file_count = Dir.glob(File.join(folder_to_count, '**', '*')).select { |file| File.file?(file) }.group_by{|f| File.size(f)}
    begin
      number_of_files = []
      file_count.each do |size, path|
        number_of_files << File.read(path.first)
        puts "#{File.read(path.first)} #{path.count}"
      end
      puts "#{BiggerNumberOfFile.get_bigger_number(file_count)}"
    end
  end

end
